<div class="company-info">
  <div class="company-logo"><img src="http://hibafood.nl/img/logo-1.jpg?1412871594" /></div>
  <div class="company-name">Hiba v.o.f.</div>
  <div class="company-address-1">Jarmuiden 33</div>
  <div class="company-address-2">1046 AC, Amsterdam</div>
  <div class="company-phone">Tel: 020 497 44 09</div>
  <div class="company-fax">Fax: 020 497 89 77</div>
  <div class="company-iban">IBAN: NL92 INGB 0007 1391 16</div>
  <div class="company-coc">KvK: 53150139</div>
  <div class="company-vat">BTW: 8507.69.188</div>
</div>

<div class="client-details">
  <div class="client-name"><?php echo $order->client->name; ?></div>
  <div class="client-address-1"><?php echo $order->client->address->street; ?> <?php echo $order->client->address->number; ?> <?php echo $order->client->address->extension; ?></div>
  <div class="client-address-2"><?php echo $order->client->address->postal_code; ?>, <?php echo $order->client->address->city; ?></div>
</div>

<h2 class="order-title">Pakbon</h2>
<div class="order-details">
  <table>
    <tr>
      <td>Bestellingsnr:<td><td><?php echo $order->getNumber(); ?><td>
    </tr>
    <tr>
      <td>Klantnr:<td><td><?php echo $order->client->getNumber(); ?><td>
    </tr>
    <tr>
      <td>Datum:<td><td><?php echo date('d-m-Y', strtotime($order->date)); ?><td>
    </tr>
  </table>
</div>
