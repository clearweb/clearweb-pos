<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

$admin = Clearworks::getEnvironment('admin');

/* Dashboard */
$admin->addPage(new \Dashboard\Page, true);


/* other admin things */
//show file page
$admin->addPage(new \Clearweb\Clearwebapps\File\ShowFilePage);

with(new MenuLoader)->init();

