<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Product\Product;
use Order\OrderLine;

class AddProductDetailsToOrderLine extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('order_line', function($t) {
            $t->string('product_name');
            $t->double('product_price');
            $t->integer('product_vat_id');
        });
        
        foreach(OrderLine::all() as $orderLine) {
            $product = Product::find($orderLine->product_id);
            
            if ( ! empty($product->name)) {
                $orderLine->product_name = $product->name;
                $orderLine->product_price = $product->price;
                $orderLine->product_vat_id = $product->vat_id;
                
                $orderLine->save();
            }
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('order_line', function($t) {
            $t->dropColumn('product_name');
            $t->dropColumn('product_price');
            $t->dropColumn('product_vat_id');
        });
	}

}
