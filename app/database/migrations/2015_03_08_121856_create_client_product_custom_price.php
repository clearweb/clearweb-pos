<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientProductCustomPrice extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('client_product_custom_price', function($table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->integer('product_id');
            
            $table->double('custom_price');
            
			$table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('client_product_custom_price');
	}

}
