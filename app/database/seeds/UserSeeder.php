<?php

use Clearweb\Clearwebapps\User\User;

class UserSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();

        User::create(array('email' => 'info@clearweb.nl', 'name'=>'Clear Web', 'password'=>\Hash::make('123456')));
    }
}