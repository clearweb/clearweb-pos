<?php

use Vat\Vat;

class VatSeeder extends Seeder {
    public function run()
    {
        DB::table('vat')->delete();

        Vat::create(array('name' => 'BTW. laag', 'percentage'=>6));
        Vat::create(array('name' => 'BTW. hoog', 'percentage'=>21));
    }
}