<?php 

return array(

             'check_login' => 'Gebruikersnaam en wachtwoord komen niet overeen',
             'check_iban' => 'Het opgegeven IBAN-nummer is niet geldig',
             'check_token' => 'U hebt een ongeldige activatielink',
             'already_active' => 'Gebruiker is reeds geactiveerd',
             'password_check' => 'Verkeerd wachtwoord ingevoerd',
             'required_if' => 'Het veld :attribute is verplicht voor makelaars',
             
	/*
	|--------------------------------------------------------------------------
	| Dutch validation language file
	|--------------------------------------------------------------------------
	|
	*/
    
	"accepted"       => "Het veld :attribute moet geaccepteerd zijn.",
	"active_url"     => "Het veld :attribute is geen geldige URL.",
	"after"          => "Het veld :attribute moet een datum na :date zijn.",
	"alpha"          => "Het veld :attribute mag alleen letters bevatten.",
	"alpha_dash"     => "Het veld :attribute mag alleen letters, nummers, onderstreep(_) en strepen(-) bevatten.",
	"alpha_num"      => "Het veld :attribute mag alleen letters en nummers bevatten.",
	"array"          => "Het veld :attribute moet geselecteerde elementen bevatten.",
	"before"         => "Het veld :attribute moet een datum voor :date zijn.",
	"between"        => array(
		"numeric" => "Het veld :attribute moet tussen :min en :max zijn.",
		"file"    => "Het veld :attribute moet tussen :min en :max kilobytes zijn.",
		"string"  => "Het veld :attribute moet tussen :min en :max karakters zijn.",
	),
	"confirmed"      => "Het veld :attribute bevestiging komt niet overeen.",
	"count"          => "Het veld :attribute moet precies :count geselecteerde elementen bevatten.",
	"countbetween"   => "Het veld :attribute moet tussen :min en :max geselecteerde elementen bevatten.",
	"countmax"       => "Het veld :attribute moet minder dan :max geselecteerde elementen bevatten.",
	"countmin"       => "Het veld :attribute moet minimaal :min geselecteerde elementen bevatten.",
	"different"      => "Het veld :attribute en :other moeten verschillend zijn.",
    "date"           => "Het veld :attribute is geen geldige datum.",
	"email"          => "Het formaat van het veld :attribute is ongeldig.",
	"exists"         => "Het gekozen :attribute is niet bekend.",
	"image"          => "Het veld :attribute moet een afbeelding zijn.",
	"in"             => "Het gekozen :attribute is ongeldig.",
	"integer"        => "Het veld :attribute moet een getal zijn.",
	"ip"             => "Het veld :attribute moet een geldig IP-adres zijn.",
	"match"          => "Het formaat van het veld :attribute is ongeldig.",
	"max"            => array(
		"numeric" => "Het veld :attribute moet minder dan :max zijn.",
		"file"    => "Het veld :attribute moet minder dan :max kilobytes zijn.",
		"string"  => "Het veld :attribute moet minder dan :max karakters zijn.",
	),
	"mimes"          => "Het veld :attribute moet een bestand zijn van het bestandstype :values.",
	"min"            => array(
		"numeric" => "Het veld :attribute mag niet kleiner dan :min zijn.",
		"file"    => "Het veld :attribute moet minimaal :min kilobytes zijn.",
		"string"  => "Het veld :attribute moet minimaal :min karakters zijn.",
	),
	"not_in"         => "Het formaat van het veld :attribute is ongeldig.",
	"numeric"        => "Het veld :attribute moet een nummer zijn.",
	"required"       => "Het veld :attribute is verplicht.",
             'required_with' => 'Het veld :attribute is verplicht',
    'regex'          => "Het formaat van het veld :attribute is ongeldig.",
	"same"           => "Het veld :attribute en :field moeten overeenkomen.",
	"size"           => array(
		"numeric" => "Het veld :attribute moet :size zijn.",
		"file"    => "Het veld :attribute moet :size kilobyte zijn.",
		"string"  => "Het veld :attribute moet :size characters zijn.",
	),
	"unique"         => "Het veld :attribute is al in gebruik.",
			 //"url"            => "Het veld :attribute formaat is ongeldig.",
			 "url"            => "De opgegeven URL is ongeldig",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute_rule" to name the lines. This helps keep your
	| custom validation clean and tidy.
	|
	| So, say you want to use a custom validation message when validating that
	| the "email" attribute is unique. Just add "email_unique" to this array
	| with your custom message. The Validator will handle the rest!
	|
	*/

	'custom' => array(
					  'new_password_retype.same' => 'wachtwoord',
                      
					  ),

	/*
	|--------------------------------------------------------------------------
	| Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as "E-Mail Address" instead
	| of "email". Your users will thank you.
	|
	| The Validator class will automatically search this array of lines it
	| is attempting to replace the :attribute place-holder in messages.
	| It's pretty slick. We think you'll like it.
	|
	*/

	'attributes' => array(
						  'subscription' => 'abonnement',
						  'issuer' => 'bank',
						  ),

);
