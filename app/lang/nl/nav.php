<?php
return array(
             'clients' => 'klanten',
             'products' => 'producten',
             'invoices' => 'facturen',
             'view_invoice' => 'factuur',
             'make_invoice' => 'factuur-maken',
             'view_order' => 'bestelling',
             'make_order' => 'bestelling-maken',
             'orders' => 'bestellingen',
             'custom_prices' => 'speciale-prijzen',
             );