<?php

return array(
			 'dashboard'=> 'dashboard',

			 
			 /* client */
			 'client'  => 'klant|klanten',
			 'name' => 'Naam',
			 
			 /* address */
			 'address' => 'adres|adressen',
			 'street'=>'straat',
			 'number'=>'nummer',
			 'house_number'=>'nummer',
			 'extension'=>'toevoeging',
			 'postal_code'=>'postcode',
			 'city'=>'stad',
			 'country'=>'land',
			 
			 /* invoice */
			 'invoice' => 'factuur|facturen',
			 'invoice_line' => 'factuurregel|factuurregels',
             'invoices_for' => 'Facturen voor :client',
			 'new_invoice_line' => 'Nieuwe factuurregel',
             'make_invoice' => 'Maak factuur',
             'make_invoice_from_orders' => 'Maak factuur van orders',
             'make_invoice_for' => 'Nieuwe factuur - :client',
             'view' => 'Bekijken',
             'invoice_with_nr' => 'Factuur :invoice',
			 'unit_price' => 'eenheidsprijs',
             'vat_percentage' => 'BTW.',
             'total_ex_vat' => 'Subtotaal',
             'total_vat' => 'BTW.',
             'total' => 'Totaal',
             'due_date' => 'Verloopdatum',
			 'mark_as_open' => 'Markeer&nbsp;als&nbsp;niet&nbsp;betaald',
			 'mark_as_open_message' => 'Weet u zeker dat factuur :invoice niet betaald is?',
			 
			 'mark_as_paid' => 'Markeer&nbsp;als&nbsp;betaald',
			 'mark_as_paid_message' => 'Weet u zeker dat factuur :invoice betaald is?',
			 
			 
			 
			 
			 /* order */
			 'order' => 'bestelling|bestellingen',
             'orders_for' => 'Bestellingen voor :client',
             'edit_order' => 'Bewerken bestelling :order - :client',
             'order_details_for' => 'Details voor bestelling :order - :client',

			 'make_order' => 'Maak bestelling',
			 'make_order_for' => 'Nieuwe bestelling - :client',
             'resume_draft_order' => 'Afmaken bestelling :order - :client',
             'publish_order' => 'order publiceren',
             'save' => 'opslaan',
             'status' => 'status',
             'date' => 'datum',
             'edit' => 'bewerken',
             
             // order line
             'order_line' => 'Orderlijn|Orderlijnen',
             'order_details' => 'Orderdetails',
             'custom_price' => 'Speciale prijs',
             'amount' => 'Aantal',
             'new_order_line' => 'orderlijn',
             'total_price' => 'Totale prijs',
             'add' => 'Toevoegen',
             
			 // order line
             'customprice' => 'Prijzen',
             'customprices_for' => 'Speciale prijzen voor :client',

			 /* product */
			 'product' => 'product|producten',
             'ean' => 'EAN-code',
			 'images' => 'afbeeldingen',
			 'price' => 'prijs',
			 'description' => 'beschrijving',
             
			 'vat' => 'BTW.',
             
			 /* action buttons */
			 'print' => 'Print',







             
             /***************************************
              * -- settings  -- 
              */
             'setting' => 'instelling|instellingen',

             // company info
             'company_info_settings' => 'Bedrijfsgegevens',
             
             'contact_settings' => 'Instellingen voor contactgegevens',

             'company' => 'bedrijfsnaam',
             'phone' => 'telefoonnummer',
             'fax' => 'faxnummer',
             'iban' => 'IBAN',
             'coc_number' => 'KvK-nr',
             'vat_number' => 'BTW-nr',
             
             // logo
             'logo_settings' => 'Instellingen voor logo',
             'logo' => 'Logo',
             
             // invoice
             'invoice_settings' => 'Instellingen voor factuur',
             'invoice_setting_footer_text' => 'Voettekst voor factuur',
             'invoice_setting_footer_text_desc' => 'Indien u deze leeg laat, zal er geen voettekst getoond worden. Gebruik <code>:due-date</code> om de verloopdatum van de factuur te tonen.',
             'invoice_due_period' => 'Binnen hoeveel dagen verloopt een factuur',
             
             'invoice_prefix' => 'Voorvoegsel voor de volgende factuur',
             'invoice_index' => 'Volgnummer voor de volgende factuur',
			 );
