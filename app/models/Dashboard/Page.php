<?php namespace Dashboard;

use \Clearweb\Clearwebapps\Layout\DashboardLayout;

use \Clearweb\Clearwebapps\Page\Page as BasePage;

class Page extends BasePage
{
	
	public function __construct() {
		$this->setSlug('dashboard');
	}
	
	public function init() {
		$this->setLayout(new DashboardLayout);
		
		parent::init();
	}
}