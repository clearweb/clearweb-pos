<?php

class MenuLoader
{
	function init() {
        $env = Clearworks::getEnvironment('admin');
        Menu::get('admin-sidebar')->addLink(trans('app.dashboard'), '/dashboard')
            ;
        
		return $this;
	}
}